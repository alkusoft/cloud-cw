package com.alkusoft.csc8110.application;

import com.alkusoft.csc8110.entity.Camera;
import com.alkusoft.csc8110.entity.VehicleEvent;
import com.alkusoft.csc8110.utils.ConfigUtils;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;
import com.microsoft.windowsazure.Configuration;
import com.microsoft.windowsazure.exception.ServiceException;
import com.microsoft.windowsazure.services.servicebus.ServiceBusContract;
import com.microsoft.windowsazure.services.servicebus.ServiceBusService;
import com.microsoft.windowsazure.services.servicebus.models.BrokeredMessage;
import com.microsoft.windowsazure.services.servicebus.models.ReceiveMessageOptions;
import com.microsoft.windowsazure.services.servicebus.models.ReceiveMode;
import com.microsoft.windowsazure.services.servicebus.models.ReceiveSubscriptionMessageResult;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.Random;

/**
 * @author Aleksander Antoniewicz
 *         Created on 03/12/2016.
 */
public class Consumer {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static CloudTable cameraTable;
    private static CloudTable vehicleTable;

    public static void main(String[] args) {

        Consumer c = new Consumer();
        try {
            c.consume();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void consume() throws IOException {
        Configuration config = ConfigUtils.getSrvcbusConfig();

        ServiceBusContract service = ServiceBusService.create(config);
        Random random = new Random();

        try {
            ReceiveMessageOptions opts = ReceiveMessageOptions.DEFAULT;
            opts.setReceiveMode(ReceiveMode.PEEK_LOCK);
            prepareDB();

            while(true)  {
                ReceiveSubscriptionMessageResult resultSubMsg = service.receiveSubscriptionMessage("TestTopic", "AllMessages", opts);
                BrokeredMessage message = resultSubMsg.getValue();
                if (message != null && message.getMessageId() != null) {
                    String messageString = IOUtils.toString(message.getBody(), "UTF-8");
                    if (messageString.contains("|")) {
                        logger.info("Found an event " + messageString);
                        TableOperation insertVehicle = TableOperation.insertOrReplace(VehicleEvent.valueOf(messageString));
                        vehicleTable.execute(insertVehicle);
                    }
                    else {
                        logger.info("Found a camera " + messageString);
                        TableOperation insertCamera = TableOperation.insertOrReplace(Camera.valueOf(messageString));
                        cameraTable.execute(insertCamera);
                    }
                    service.deleteMessage(message);
                }
                else {
                    logger.info("No more messages.");
                    Thread.sleep(random.nextInt(4000));
                }
            }
        }
        catch (ServiceException | StorageException e) {
            logger.error(e.getMessage());
        } catch (InterruptedException | IOException e) {
            logger.error(e.getMessage());
        }
    }

    private void prepareDB() throws IOException {
        try {
            CloudStorageAccount storageAccount =
                    CloudStorageAccount.parse(ConfigUtils.getStorageConfig());

            CloudTableClient tableClient = storageAccount.createCloudTableClient();
            cameraTable = tableClient.getTableReference("cameras");
            cameraTable.createIfNotExists();
            vehicleTable = tableClient.getTableReference("vehicles");
            vehicleTable.createIfNotExists();
        } catch (StorageException | URISyntaxException | InvalidKeyException e) {
            logger.error(e.getMessage());
        }
    }
}
