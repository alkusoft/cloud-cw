package com.alkusoft.csc8110.application;

import com.alkusoft.csc8110.entity.SpeedingVehicleEvent;
import com.alkusoft.csc8110.utils.ConfigUtils;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.queue.CloudQueue;
import com.microsoft.azure.storage.queue.CloudQueueClient;
import com.microsoft.azure.storage.queue.CloudQueueMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Aleksander Antoniewicz
 *         Created on 13/12/2016.
 */
public class VehicleCheck {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private CloudQueue queue;
    public static void main(String[] args) {
        VehicleCheck check = new VehicleCheck();
        check.prepareQueue();
        check.fetchMessages();
    }

    private void fetchMessages() {
        try {
            while (true) {
                for (CloudQueueMessage message : queue.retrieveMessages(1, 20, null, null)) {
                    if (message != null && message.getMessageId() != null) {
                        SpeedingVehicleEvent event = SpeedingVehicleEvent.valueOf(message.getMessageContentAsString());
                        boolean stolen = isVehicleStolen(event.getRegistration());
                        logger.info("Vehicle " + event.getRegistration() +  " is stolen: " + stolen);
                        queue.deleteMessage(message);
                    }
                }
            }
        } catch (StorageException e) {
            logger.error(e.getMessage());
        }
    }

    private void prepareQueue() {
        try {
            CloudStorageAccount storageAccount =
                    CloudStorageAccount.parse(ConfigUtils.getStorageConfig());
            CloudQueueClient queueClient = storageAccount.createCloudQueueClient();
            queue = queueClient.getQueueReference("speedingqueue");
            queue.createIfNotExists();
        } catch (Exception e) {
            // Output the stack trace.
            e.printStackTrace();
        }
    }

    private boolean isVehicleStolen(String vehicleRegistration) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return (Math.random() > 0.95);
    }
}
