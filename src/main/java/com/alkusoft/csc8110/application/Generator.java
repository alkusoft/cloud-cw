package com.alkusoft.csc8110.application;

import com.alkusoft.csc8110.entity.Camera;
import com.alkusoft.csc8110.entity.VehicleEvent;
import com.alkusoft.csc8110.utils.ConfigUtils;
import com.microsoft.windowsazure.Configuration;
import com.microsoft.windowsazure.exception.ServiceException;
import com.microsoft.windowsazure.services.servicebus.ServiceBusContract;
import com.microsoft.windowsazure.services.servicebus.ServiceBusService;
import com.microsoft.windowsazure.services.servicebus.models.*;
import com.mifmif.common.regex.Generex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Aleksander Antoniewicz
 *         Created on 03/12/2016.
 */
public class Generator {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private static final String[] vehicleType = {"Car", "Truck", "Motorcycle"};
    private final List<VehicleEvent> events = new ArrayList<>();

    public static void main(String[] args) {
        Generator g = new Generator();
        try {
            g.generate(args);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void generate(String[] args) throws IOException {
        Configuration config = ConfigUtils.getSrvcbusConfig();

        Camera camera = new Camera(args[0], args[1], args[2], Integer.parseInt(args[3]), LocalDateTime.now());

        ServiceBusContract service = ServiceBusService.create(config);
        // generation rate
        int rate = Integer.parseInt(args[4]);

        if (rate < 1)
            rate = 1;

        Generex generex = new Generex("[A-Z]{2}[0-9]{2}_[A-Z]{3}");
        Random random = new Random();
        BrokeredMessage cameraMessage = new BrokeredMessage(camera.toString());
        createSub(service, camera);
        sendMessage(cameraMessage, service);

        while(true) {
            VehicleEvent event = generateVehicle(generex, getVehicleType(random), getSpeed(random, camera.getMaxSpeed()), camera.getId());
            logger.info("VPE: " + event.toString());
            BrokeredMessage message = new BrokeredMessage(event.toString());
            message.setProperty("speed", event.getSpeed());
            message.setProperty("name", event.getCameraID());
            sendMessage(message, service);
            events.add(event);
            try {
                Thread.sleep(rate * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendMessage(BrokeredMessage message, ServiceBusContract service) {
        try {
            service.sendTopicMessage("TestTopic", message);
        }
        catch (ServiceException e) {
            logger.error(e.getMessage());
        }
    }

    private int getSpeed(Random random, int cameraSpeed) {
        int max = (int) (cameraSpeed * 1.5);
        int min = (int) (cameraSpeed * 0.5);
        return random.nextInt((max - min) + 1) + min;
    }

    private static String getVehicleType(Random random) {
        return vehicleType[random.nextInt(vehicleType.length)];
    }

    private VehicleEvent generateVehicle(Generex generex, String vehicleType, int speed, String cameraID) {
        return new VehicleEvent(generex.random(), vehicleType, speed, cameraID);
    }

    private void createSub(ServiceBusContract service, Camera camera) {
        try {
            SubscriptionInfo subInfo = new SubscriptionInfo(camera.getId());
            ListSubscriptionsResult getResult = service.listSubscriptions("TestTopic");
            boolean found = false;
            for (SubscriptionInfo info : getResult.getItems()) {
                if (info.getName().equals(camera.getId()))
                    found = true;
            }

            if (!found) {
                CreateSubscriptionResult result = service.createSubscription("TestTopic", subInfo);
                RuleInfo ruleInfo = new RuleInfo(camera.getId());
                ruleInfo = ruleInfo.withSqlExpressionFilter("speed > " + camera.getMaxSpeed() + " AND name='" + camera.getId() + "'");
                CreateRuleResult ruleResult = service.createRule("TestTopic", camera.getId(), ruleInfo);
                service.deleteRule("TestTopic", camera.getId(), "$Default");
            }
        } catch (ServiceException e) {
            e.printStackTrace();
        }
    }
}
