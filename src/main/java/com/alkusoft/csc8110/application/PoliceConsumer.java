package com.alkusoft.csc8110.application;

import com.alkusoft.csc8110.entity.Camera;
import com.alkusoft.csc8110.entity.SpeedingVehicleEvent;
import com.alkusoft.csc8110.entity.VehicleEvent;
import com.alkusoft.csc8110.utils.ConfigUtils;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.queue.CloudQueue;
import com.microsoft.azure.storage.queue.CloudQueueClient;
import com.microsoft.azure.storage.queue.CloudQueueMessage;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;
import com.microsoft.windowsazure.exception.ServiceException;
import com.microsoft.windowsazure.services.servicebus.ServiceBusContract;
import com.microsoft.windowsazure.services.servicebus.ServiceBusService;
import com.microsoft.windowsazure.services.servicebus.models.BrokeredMessage;
import com.microsoft.windowsazure.services.servicebus.models.ReceiveMessageOptions;
import com.microsoft.windowsazure.services.servicebus.models.ReceiveMode;
import com.microsoft.windowsazure.services.servicebus.models.ReceiveSubscriptionMessageResult;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 * @author Aleksander Antoniewicz
 *         Created on 05/12/2016.
 */
public class PoliceConsumer {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private CloudTable speedingVehicles;
    private CloudQueue queue;

    public PoliceConsumer() {
        prepareDB();
        prepareQueue();
    }

    public static void main(String[] args) {
        PoliceConsumer consumer = new PoliceConsumer();
        consumer.retrieveSpeedingsForCamera(args[0]);
    }

    private Optional<Camera> getCamera(String cameraName) {
        Reader reader = new Reader();
        List<Camera> cameras = reader.getCameras();
        return cameras.stream().filter(o -> o.getId().equals(cameraName)).findFirst();
    }

    private void retrieveSpeedingsForCamera(String cameraName) {
        Optional<Camera> cameraOpt = getCamera(cameraName);
        Camera camera;
        if (cameraOpt.isPresent()) {
            camera = cameraOpt.get();
        }
        else throw new RuntimeException("No camera found with this ID");
        try {
            readSpeedingVehicles(camera);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void readSpeedingVehicles(Camera camera) throws IOException {
        ServiceBusContract service = ServiceBusService.create(ConfigUtils.getSrvcbusConfig());
        Random random = new Random();
        ReceiveMessageOptions opts = ReceiveMessageOptions.DEFAULT;
        opts.setReceiveMode(ReceiveMode.PEEK_LOCK);

        try {
            while (true) {
                ReceiveSubscriptionMessageResult resultSubMsg = service.receiveSubscriptionMessage("TestTopic", camera.getId(), opts);
                BrokeredMessage message = resultSubMsg.getValue();
                if (message != null && message.getMessageId() != null) {
                    String messageString = IOUtils.toString(message.getBody(), "UTF-8");

                    VehicleEvent event = VehicleEvent.valueOf(messageString);
                    SpeedingVehicleEvent sve;
                    if (event.getSpeed() > (camera.getMaxSpeed() * 1.1)) {
                        logger.info(messageString + " PRIORITY");
                        sve = new SpeedingVehicleEvent(event, true);
                    }
                    else {
                        logger.info(messageString);
                        sve = new SpeedingVehicleEvent(event, false);
                    }

                    TableOperation insertSpeedingVehicle = TableOperation.insertOrReplace(sve);
                    speedingVehicles.execute(insertSpeedingVehicle);
                    CloudQueueMessage qMessage = new CloudQueueMessage(sve.toString());
                    queue.addMessage(qMessage);
                    service.deleteMessage(message);
                } else {
                    logger.info("No more messages.");
                    Thread.sleep(random.nextInt(4000));
                }
            }
        } catch (ServiceException | StorageException | InterruptedException | IOException e) {
            logger.error(e.getMessage());
        }
    }

    private void prepareDB() {
        try {
            CloudStorageAccount storageAccount =
                    CloudStorageAccount.parse(ConfigUtils.getStorageConfig());
            CloudTableClient tableClient = storageAccount.createCloudTableClient();
            speedingVehicles = tableClient.getTableReference("SpeedingVehicles");
            speedingVehicles.createIfNotExists();
        } catch (StorageException | URISyntaxException | InvalidKeyException | IOException e) {
           logger.error(e.getMessage());
        }
    }

    private void prepareQueue() {
        try {
            CloudStorageAccount storageAccount =
                    CloudStorageAccount.parse(ConfigUtils.getStorageConfig());
            CloudQueueClient queueClient = storageAccount.createCloudQueueClient();
            queue = queueClient.getQueueReference("speedingqueue");
            queue.createIfNotExists();
        } catch (Exception e) {
            // Output the stack trace.
            e.printStackTrace();
        }
    }
}
