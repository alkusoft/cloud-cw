package com.alkusoft.csc8110.application;

import com.alkusoft.csc8110.entity.Camera;
import com.alkusoft.csc8110.entity.SpeedingVehicleEvent;
import com.alkusoft.csc8110.utils.ConfigUtils;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Aleksander Antoniewicz
 *         Created on 04/12/2016.
 */
public class Reader {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private CloudTable cameraTable;
    private CloudTable speedingVehicles;

    public Reader() {
        prepareDB();
    }

    public List<Camera> getCameras() {
        TableQuery<Camera> partitionQuery = TableQuery.from(Camera.class);
        List<Camera> cameras = new ArrayList<>();
        for (Camera c : cameraTable.execute(partitionQuery)) {
            cameras.add(c);
        }
        return cameras;
    }

    public List<SpeedingVehicleEvent> getSpeedingVehicles() {
        String priorityFilter = TableQuery.generateFilterCondition(
                "PartitionKey",
                TableQuery.QueryComparisons.EQUAL,
                "true");
        TableQuery<SpeedingVehicleEvent> partitionQuery = TableQuery.from(SpeedingVehicleEvent.class).where(priorityFilter);
        List<SpeedingVehicleEvent> sev = new ArrayList<>();
        for (SpeedingVehicleEvent s : speedingVehicles.execute(partitionQuery)) {
            sev.add(s);
        }
        return sev;
    }

    private void prepareDB() {
        try {
            CloudStorageAccount storageAccount =
                    CloudStorageAccount.parse(ConfigUtils.getStorageConfig());

            CloudTableClient tableClient = storageAccount.createCloudTableClient();
            cameraTable = tableClient.getTableReference("cameras");
            speedingVehicles = tableClient.getTableReference("SpeedingVehicles");
        } catch (StorageException | URISyntaxException | InvalidKeyException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Reader reader = new Reader();
        if (args[0].equals("cameras")) {
            List<Camera> cameras = reader.getCameras();
            for (Camera c : cameras) {
                System.out.println(c.toString());
            }

        } else if (args[0].equals("speeders")) {
            List<SpeedingVehicleEvent> sevs = reader.getSpeedingVehicles();
            for (SpeedingVehicleEvent event : sevs) {
                System.out.println(event.toString());
            }
        }
    }
}
