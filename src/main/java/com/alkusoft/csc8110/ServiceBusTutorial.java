package com.alkusoft.csc8110;

import com.microsoft.windowsazure.Configuration;
import com.microsoft.windowsazure.exception.ServiceException;
import com.microsoft.windowsazure.services.servicebus.ServiceBusConfiguration;
import com.microsoft.windowsazure.services.servicebus.ServiceBusContract;
import com.microsoft.windowsazure.services.servicebus.ServiceBusService;
import com.microsoft.windowsazure.services.servicebus.models.BrokeredMessage;

/**
 * @author Aleksander Antoniewicz
 */
public class ServiceBusTutorial {

    public static void main(String[] args) {
        ServiceBusTutorial tutorial = new ServiceBusTutorial();
        tutorial.startTutorial();
    }

    private void startTutorial() {
        Configuration config =
                ServiceBusConfiguration.configureWithSASAuthentication(
                        "srvcbus",
                        "RootManageSharedAccessKey",
                        "9fJpaf5iMOhqpvQxIYEsQmXYiWk/HNnW6ubmoYjmebA=",
                        ".servicebus.windows.net"
                );

        ServiceBusContract service = ServiceBusService.create(config);
        BrokeredMessage message = new BrokeredMessage("MyMessage");
        try {
            service.sendTopicMessage("TestTopic", message);
        }
        catch (ServiceException e) {
            System.out.print("ServiceException encountered: ");
            System.out.println(e.getMessage());
            System.exit(-1);
        }



    }

}
