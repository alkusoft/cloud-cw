package com.alkusoft.csc8110.entity;

import com.microsoft.azure.storage.table.TableServiceEntity;

/**
 * @author Aleksander Antoniewicz
 *         Created on 09/12/2016.
 */
public class SpeedingVehicleEvent extends TableServiceEntity {
    private Boolean priority;
    private String registration;
    private String type;
    private int speed;
    private String cameraID;


    public SpeedingVehicleEvent(String registration, String type, int speed, String cameraID, Boolean priority) {
        this.registration = registration;
        this.type = type;
        this.speed = speed;
        this.cameraID = cameraID;
        this.rowKey = registration + cameraID + speed;
        this.partitionKey = Boolean.toString(priority);
        this.priority = priority;
    }

    public SpeedingVehicleEvent(VehicleEvent ve, Boolean priority) {
        this.registration = ve.getRegistration();
        this.type = ve.getType();
        this.speed = ve.getSpeed();
        this.cameraID = ve.getCameraID();
        this.rowKey = registration + cameraID + speed;
        this.partitionKey = Boolean.toString(priority);
        this.priority = priority;
    }

    public SpeedingVehicleEvent() {
    }

    public boolean isPriority() {
        return priority;
    }

    public void setPriority(Boolean priority) {
        this.priority = priority;
    }

    public Boolean getPriority() {
        return priority;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getCameraID() {
        return cameraID;
    }

    public void setCameraID(String cameraID) {
        this.cameraID = cameraID;
    }

    public static SpeedingVehicleEvent valueOf(String valueOf) {
        String[] input = valueOf.split("\\|");
        return new SpeedingVehicleEvent(input[0], input[1],
                Integer.parseInt(input[2]), input[3], Boolean.parseBoolean(input[4]));
    }

    @Override
    public String toString() {
        return getRegistration() + '|' + getType()
                + '|' + getSpeed() + '|' + getCameraID() + '|' + priority;
    }
}
