package com.alkusoft.csc8110.entity;

import com.microsoft.azure.storage.table.TableServiceEntity;


/**
 * @author Aleksander Antoniewicz
 *         Created on 03/12/2016.
 */
public class VehicleEvent extends TableServiceEntity{
    private String registration;
    private String type;
    private int speed;
    private String cameraID;

    public VehicleEvent(String registration, String type, int speed, String cameraID) {
        this.registration = registration;
        this.type = type;
        this.speed = speed;
        this.cameraID = cameraID;
        this.rowKey = registration + cameraID + speed;
        this.partitionKey = "vehicle";
    }

    public VehicleEvent() {
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getCameraID() {
        return cameraID;
    }

    public void setCameraID(String cameraID) {
        this.cameraID = cameraID;
    }

    @Override
    public String toString() {
        return registration + '|' + type + '|' + speed + '|' + cameraID;
    }

    public static VehicleEvent valueOf(String valueOf) {
        String[] input = valueOf.split("\\|");
        return new VehicleEvent(input[0], input[1], Integer.parseInt(input[2]), input[3]);
    }
}
