package com.alkusoft.csc8110.entity;

import com.alkusoft.csc8110.utils.DateUtils;
import com.microsoft.azure.storage.table.TableServiceEntity;

import java.time.LocalDateTime;

/**
 * @author Aleksander Antoniewicz
 *         Created on 03/12/2016.
 */
public class Camera extends TableServiceEntity {
    private String id;
    private String street;
    private String town;
    private int maxSpeed;
    private String initTS;

    public Camera(String id, String street, String town, int maxSpeed, LocalDateTime initDate) {
        this.id = id;
        this.street = street;
        this.town = town;
        this.maxSpeed = maxSpeed;
        this.initTS = DateUtils.formatter.format(initDate);
        this.partitionKey = "camera";
        this.rowKey = id + initTS;
    }

    public Camera(String id, String street, String town, int maxSpeed, String initDate) {
        this.id = id;
        this.street = street;
        this.town = town;
        this.maxSpeed = maxSpeed;
        this.initTS = initDate;
        this.partitionKey = "camera";
        this.rowKey = id + initTS;
    }

    public Camera() {
    }

    public String getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInitTS() {
        return initTS;
    }

    public void setInitTS(String initTS) {
        this.initTS = initTS;
    }

    @Override
    public String toString() {
        return id + ',' + street + ',' + town + ',' + maxSpeed + "," + initTS;
    }

    public static Camera valueOf(String cameraString) {
        String[] input = cameraString.split(",");
        return new Camera(input[0], input[1], input[2], Integer.parseInt(input[3]), input[4]);
    }
}
