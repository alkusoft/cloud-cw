package com.alkusoft.csc8110.utils;

import java.time.format.DateTimeFormatter;

/**
 * @author Aleksander Antoniewicz
 *         Created on 03/12/2016.
 */
public class DateUtils {
    public static final DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
}
