package com.alkusoft.csc8110.utils;

import com.microsoft.windowsazure.Configuration;
import com.microsoft.windowsazure.services.servicebus.ServiceBusConfiguration;
import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

/**
 * @author Aleksander Antoniewicz
 *         Created on 10/12/2016.
 */
public class ConfigUtils {
    private final static String FILE_PATH = "src/main/resources/config.conf";

    public static Configuration getSrvcbusConfig() throws IOException {
        List<String> configDetails = IOUtils.readLines(new FileInputStream(FILE_PATH));

        Configuration config = ServiceBusConfiguration.configureWithSASAuthentication(
                        configDetails.get(0),
                        configDetails.get(1),
                        configDetails.get(2),
                        configDetails.get(3));
        return config;
    }

    public static String getStorageConfig() throws IOException {
        List<String> configDetails = IOUtils.readLines(new FileInputStream(FILE_PATH));
        return configDetails.get(4);
    }
}
